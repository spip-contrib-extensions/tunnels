# Tunnels : conception

À visualiser avec Graphviz

```graphviz
digraph tunnels {

  rankdir="LR"
  nodesep=0.25
  //splines=ortho
  node [fontname=Courier,shape=box]
  edge [style=dashed]

  html [label="tunnel.html", fontcolor=Blue, color=Blue, shape=circle]
  pipeline [label="pipeline"]

  // relations
  html->infos [color="blue", style="solid"]
  infos->{determiner_tunnel,determiner_etape,etape_precedente,etape_suivante} [constraint = true]
  decrire_tunnels->{pipeline}
  determiner_tunnel->{decrire_tunnels}
  lister_etapes->{decrire_tunnels}
  determiner_etape->{lister_etapes}
  //titre->{decrire_tunnels}
  etape_suivante->{lister_etapes}
  etape_precedente->{lister_etapes}

  // choses au même niveau
  {rank=same;determiner_tunnel determiner_etape etape_precedente etape_suivante}
}
```
