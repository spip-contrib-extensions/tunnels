<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tunnels_nom' => 'Tunnels',
	'tunnels_slogan' => 'Implémenter facilement des tunnels.',
	'tunnels_description' => 'Un tunnel est une série d’étapes à faire dans un ordre précis. C’est en  quelque sorte l’équivalent d’un formulaire multi-étapes, mais fait sur plusieurs pages avec plusieurs formulaires indépendants. Il peut s’agir de tunnels pour passer une commande, pour s’abonner, etc. Ce plugin fournit un canevas pour en implémenter facilement.',

);
